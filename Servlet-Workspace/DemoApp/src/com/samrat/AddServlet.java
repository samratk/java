package com.samrat;


import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
@WebServlet("/add")
public class AddServlet extends HttpServlet {

	
	//public void service(HttpServletRequest req, HttpServletResponse res) throws IOException {
	//	int s=Integer.parseInt(req.getParameter("num1")) + Integer.parseInt(req.getParameter("num2"));
	//	res.getWriter().println("The sum of the numbers entered is Service " + s);
	//	
	//}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		int i = Integer.parseInt(req.getParameter("num1"));
		int j = Integer.parseInt(req.getParameter("num2"));
		int s= i+j ;
		res.getWriter().println("The sum of the numbers entered is Post " + s);
		
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		int i = Integer.parseInt(req.getParameter("num1"));
		int j = Integer.parseInt(req.getParameter("num2"));
		int s= i+j ;
		res.getWriter().println("The sum of the numbers entered is Post " + s);
		
		//setting attributes to request 
		//req.setAttribute("k", s);
		//dispatching from add to product 
		//RequestDispatcher rd = req.getRequestDispatcher("prod"); 
		//rd.forward(req,res);
		
		//doing re-direction - URL re-direction. 
		res.sendRedirect("prod?num1=" + i + "&num2=" + j);
		
	}
	

}
