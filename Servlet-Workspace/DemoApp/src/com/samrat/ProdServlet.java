package com.samrat;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/prod")
public class ProdServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		int p=Integer.parseInt(req.getParameter("num1")) * Integer.parseInt(req.getParameter("num2"));
		res.getWriter().println("The product of the numbers entered is Get " + p);// + req.getAttribute("k"));
		
		//redirected. not fetching the res or req
		res.getWriter().println("SAMRAT KAR");
		
	}
}
