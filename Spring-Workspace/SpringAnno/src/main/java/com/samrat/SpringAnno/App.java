package com.samrat.SpringAnno;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		System.out.println("In main!");
		// OPTION 1 - using the object directly in the code.
		Vehicle obj1 = new Car();
		obj1.drive();

		//Using beans created from java configuration file AppConfig
		ApplicationContext context1 = new AnnotationConfigApplicationContext(AppConfig.class);
		Vehicle obj2 = (Vehicle) context1.getBean(Car.class);
		obj2.drive();

		ApplicationContext context2 = new AnnotationConfigApplicationContext(AppConfig.class);
		Vehicle obj3 = (Vehicle) context2.getBean(Bike.class);
		obj3.drive();

		ApplicationContext context3 = new AnnotationConfigApplicationContext(AppConfig.class);
		Tyre obj4 = (Tyre) context3.getBean(Tyre.class);
		System.out.println(obj4);

	}
}
