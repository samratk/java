package com.samrat.SpringAnno;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class AppAuto {
	public static void main(String[] args) {
		System.out.println("In main!");
		// OPTION 1 - using the object directly in the code.
		Vehicle obj1 = new Car();
		obj1.drive();
		
		// Using dependency injection using autoconfig without any beans. 
		ApplicationContext context4 = new AnnotationConfigApplicationContext(AutoAppConfig.class);
		Vehicle obj5 = (Vehicle) context4.getBean(Car.class);
		obj5.drive();

	}
}
