package com.samrat.SpringAnno;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration	
public class AppConfig {
	@Bean
	public Car produce_car() {
		return new Car();
	}
	@Bean
	public Bike produce_bike() {
		return new Bike();
	}
	@Bean 
	public Tyre produce_tyre() {
		return new Tyre();
	}
}
