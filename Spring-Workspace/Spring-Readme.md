<span style="color:; font-family:Menlo, Consolas, DejaVu Sans Mono, monospace; font-size: 12pt ;">

<h4> 1. DEPENDENCY INJECTION USING SPRING FRAMEWORK USING XML - WITH AND WITHOUT ANNOTATIONS </h4>

- Setup - Create a new Maven project, and update the pom.xml with the following latest Spring dependency -
```
    <dependency>
    	<groupId>org.springframework</groupId>
    	<artifactId>spring-context</artifactId>
     	<version>5.2.8.RELEASE</version>
    </dependency>

```
  `Example Project - firstMavenPkg`

- `Option 1 : No dependency injection.`
- `Option 2 - Dependency injection with xml based beans configuration. No properties setting`
  Mapping strings from the bean xml file to select which object to be created. Dependency injected using xml file - object factory. Note - Following is the bean definition which is to be added in the xml -
```
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.springframework.org/schema/beans
    http://www.springframework.org/schema/beans/spring-beans-4.0.xsd">

```
Note that there is no property injection yet. The brand of the Tyre is null. Hence the output looks like -
```
Inside Car with tyre : null
Inside Bike with tyre : null

```

- `Option 3 - Dependency injection with xml based beans configuration. Property injection in xml with property tag.`
  Property injection using xml. Note : You need to have a `default constructor` for this to work!

- `Option 4 - Dependency injection with xml based beans configuration. Property injection in xml using constructor tag.`
  Note : You need to have `constructor overloaded` to assign the properties intended. Here in the example, Tyre class's constructor is overloaded to assign the brand property.

- `Option 5 - Dependency injection without xml based beans. Only with annotation based beans.` No property injection. Only Java `Annotation`.
Note - the bean string is same as the name of the class with all small letters. This helped us to do away with xml altogether. Objects are selectively being created based on a string that can be passed to the main application from an external source. This helps us to hide the business model (name of the class and hence the class header files) from the user. Just using the @Component annotation with the class.

Note - In both the above cases, property injection is done. So the output shows the brand of the Tyre -

```
Tyre [brand=MRF - Brand with Property Injection via xml]
Tyre [brand=CEAT - Brand with constructor injection via xml]

```
- `Option 6 - Dependency injection without xml based beans with property injection`
  This is done with the annotation `Autowiring` is done to connect the properties too for the classes. Now the new annotation based xml is being used. Note the extra lines added in the file - `spring-annot.xml`.

Note - Now the output shows that the Tyre is instantiated in the Car and Bike objects -

```
Inside Car with tyre : Tyre [brand=null]
Inside Bike with tyre : Tyre [brand=null]
```

<h4> 2. DEPENDENCY INJECTION USING SPRING FRAMEWORK WITHOUT XML USING JAVA CONFIGURATION FILES WITH ANNOTATIONS </h4>

`Example Project - SpringAnno`
1. Use @Beans to mark the beans in a Config file. Ex - AppConfig.java.
2. change the context class from ClassPathXmlApplicationContext to AnnotationConfigApplicationContext.
3. Auto Configuration of beans can also be done using the annotation in the config file as @ComponentScan.
4. Note that both autoscan and manual beans cannot be done in the same package, as beans are global to the entire package. There will multiple beans with the same class return type. Hence when I run auto config, I am having to comment out the manually written Bean annotations in the AppConfig. 
