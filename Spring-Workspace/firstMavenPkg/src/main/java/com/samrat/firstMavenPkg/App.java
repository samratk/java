package com.samrat.firstMavenPkg;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "In main!" );
        //OPTION 1 - using the object directly in the code. 
        Vehicle obj1 = new Car();
        obj1.drive();
        
        //OPTION 2 - mapping from the Bean xml file. 
        ApplicationContext context1 = new ClassPathXmlApplicationContext("spring.xml");
        Vehicle obj2 = (Vehicle)context1.getBean("vehicle");
        obj2.drive();
        
        //OPTION 3 - Property injection in xml. 
        //Tyre is xml  based bean for this example  
        ApplicationContext context3 = new ClassPathXmlApplicationContext("spring-prop-injection.xml");
        Tyre tyre1 = (Tyre)context3.getBean("tyre");
        System.out.println(tyre1);
        
        //OPTION 4 - Constuctor  injection in xml. 
        //Tyre is xml  based bean for this example  
        ApplicationContext context4 = new ClassPathXmlApplicationContext("spring-const-injection.xml");
        Tyre tyre2 = (Tyre)context4.getBean("tyre");
        System.out.println(tyre2);
        
        //Option 5 - Annotation based with xml but no beans. No property injection yet.  
        ApplicationContext context5 = new ClassPathXmlApplicationContext("spring-annot.xml");
        Vehicle obj5 = (Vehicle)context5.getBean("car"); 
        obj5.drive();
        
        //Option 6 - Annotation based without any xml. With property injection using Autowiring.
        ApplicationContext context6 = new ClassPathXmlApplicationContext("spring-annot.xml");
        Vehicle obj6 = (Vehicle)context6.getBean("bike"); 
        obj6.drive();
        

                
    }
}
