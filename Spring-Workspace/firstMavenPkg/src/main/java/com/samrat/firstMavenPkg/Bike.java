package com.samrat.firstMavenPkg;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Bike implements Vehicle{
	@Autowired
	private Tyre tyre;
	
	public Bike() {
	}
	
	public Bike(Tyre tyre) {
		super();
		this.tyre = tyre;
	}

	public Tyre getTyre() {
		return tyre;
	}

	public void setTyre(Tyre tyre) {
		this.tyre = tyre;
	}

	public void drive() {
		System.out.println("Inside Bike with tyre : " + tyre);
	}

	@Override
	public String toString() {
		return "Bike [tyre=" + tyre + "]";
	}

}
